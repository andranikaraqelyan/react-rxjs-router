'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Link = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Router = require('./Router');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Link extends _react2.default.Component {

    constructor(props) {
        super(props);

        this.state = {
            path: ''
        };

        _Router.Router.subscribe(path => this.setState({ path }));

        this.clickHandler = this.clickHandler.bind(this);
    }

    clickHandler(event) {

        event.preventDefault();
        _Router.Router.redirect(this.props.to);
    }

    render() {

        let className = this.props.className ? this.props.className : '';
        className += this.props.activeClassName && this.state.path.startsWith(this.props.to) ? ' ' + String(this.props.activeClassName) : '';

        return _react2.default.createElement(
            'a',
            {
                href: this.props.to,
                onClick: this.clickHandler,
                className: className
            },
            this.props.children
        );
    }

}

exports.Link = Link;