import React from 'react';
import {Router} from "./Router";

class Link extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            path: ''
        };

        Router.subscribe( path => this.setState({path}) );

        this.clickHandler = this.clickHandler.bind(this);
    }

    clickHandler(event){

        event.preventDefault();
        Router.redirect(this.props.to);

    }

    render(){

        let className = this.props.className?this.props.className:'';
        className +=
            this.props.activeClassName &&
            this.state.path.startsWith(this.props.to)?(' ' + String(this.props.activeClassName)):'';

        return (
            <a
                href={this.props.to}
                onClick={this.clickHandler}
                className={className}
            >
                {this.props.children}
            </a>
        );

    }

}

export {Link};