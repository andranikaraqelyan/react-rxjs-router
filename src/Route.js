import {Router} from './Router';
import React from 'react';

class Route extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            path: ''
        };

        this.initRegExp();

        this.path_regexp = new RegExp(this.regexp_string);
        this.exact_path_regexp = new RegExp(this.regexp_string + '$');

        Router.subscribe(path=>{
            this.setState({
                path
            });
        });
    }

    initRegExp(){
        /*  @indev  */
        let path = this.props.path;

        this.regexp_string = '';
        if(!/\/:\w+/.test(path)){
            this.regexp_string = path;
            return;
        }

        this.params = [];
        while(path){
            const match = path.match(/\/:\w+/);
            const index = match.index;
            const paramStr = match[0];
            path = path.slice(index+paramStr.length);
            this.params.push(paramStr.slice(2));
            this.regexp_string += match.input.slice(0,index);
            this.regexp_string += '(/\\w+)';
        }
        this.regexp_string = `^` + this.regexp_string;
    }

    render(){

        const regexp = this.props.exact?this.exact_path_regexp:this.path_regexp;

        const match = this.state.path.match(regexp);
        const childProps = {
            routing: {
                params: {}
            }
        };
        if(match && this.params){
            this.params.forEach((v,index)=>{
                childProps.routing.params[v] = match[index+1].slice(1);
            });
        }
        return this.props.component && match?React.createElement(this.props.component,childProps):null;

    }
}

export {Route};